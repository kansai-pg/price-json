from bs4 import BeautifulSoup
import selenium 
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service
import re
import json

options = selenium.webdriver.ChromeOptions()
options.add_argument('--headless')
options.add_argument('--no-sandbox')
options.add_argument("--disable-gpu")
options.add_argument("--disable-dev-shm-usage")
options.add_argument("--disable-dev-tools")
options.add_argument("--hide-scrollbars")
options.add_argument("--single-process")
options.add_argument("--window-size=880x996")
options.binary_location = '/usr/bin/google-chrome'

service = Service(executable_path='/usr/src/app/node_modules/chromedriver/bin/chromedriver')

driver = selenium.webdriver.Chrome(options=options, service=service)

driver.get('https://charahiroba.com/prize/calendar/')
WebDriverWait(driver=driver, timeout=30).until(EC.presence_of_all_elements_located)
#print(driver.page_source)
soup = BeautifulSoup(driver.page_source, 'html.parser')

names = soup.find_all(class_='calendar__text')
dates = soup.find_all(class_='calendar__title')

req_data = []
#https://qiita.com/shimajiroxyz/items/1ecbae20cb5173761f4f
for (date, name) in zip(dates, names):
    req_data += [{
    "date": re.sub(r"\D","", date.text),
    "name": re.sub(r"[\u3000 \t \r\n]","", name.text)
    }]


with open("./public/furyu.json",'w') as f:
    json.dump(req_data, f, ensure_ascii=False, indent=4)

driver.quit()
