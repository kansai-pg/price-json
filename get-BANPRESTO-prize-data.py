from bs4 import BeautifulSoup
import selenium 
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service
import re
import json
import datetime

options = selenium.webdriver.ChromeOptions()
options.add_argument('--headless')
options.add_argument('--no-sandbox')
options.add_argument("--disable-gpu")
options.add_argument("--disable-dev-shm-usage")
options.add_argument("--disable-dev-tools")
options.add_argument("--hide-scrollbars")
options.add_argument("--single-process")
options.add_argument("--window-size=880x996")
options.binary_location = '/usr/bin/google-chrome'

service = Service(executable_path='/usr/src/app/node_modules/chromedriver/bin/chromedriver')

driver = selenium.webdriver.Chrome(options=options, service=service)

now = datetime.datetime.now()
url = 'https://bsp-prize.jp/schedule/?d=' + str( now.strftime("%Y-%m") )

driver.get(url)
WebDriverWait(driver=driver, timeout=30).until(EC.presence_of_all_elements_located)
#print(driver.page_source)
soup = BeautifulSoup(driver.page_source, 'html.parser')
#ブラウザの開発ツールから頑張ってclass名を探す必要あり
names = soup.find_all(class_='products_name')
dates = soup.find_all(class_='products_date')
#driver.get じゃうまくいかない
for i in range(2):
    driver.find_element(By.CLASS_NAME,"pager_next").click()
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    names += soup.find_all(class_='products_name')
    dates += soup.find_all(class_='products_date')

req_data = []
for (date, name) in zip(dates, names):
    #print(datetime.strptime(re.sub(r"\D","", date.text), '%Y%m%d').date(),name.text)
    req_data += [{
    "date": re.sub(r"\D","", date.text),
    "name": name.text.replace('\u3000', '')
    }]

with open("./public/BANPRESTO.json",'w') as f:
    json.dump(req_data, f, ensure_ascii=False, indent=4)

driver.quit()
