from bs4 import BeautifulSoup
import selenium 
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service
import re
import json

options = selenium.webdriver.ChromeOptions()
options.add_argument('--headless')
options.add_argument('--no-sandbox')
options.add_argument("--disable-gpu")
options.add_argument("--disable-dev-shm-usage")
options.add_argument("--disable-dev-tools")
options.add_argument("--hide-scrollbars")
options.add_argument("--single-process")
options.add_argument("--window-size=880x996")
options.binary_location = '/usr/bin/google-chrome'

service = Service(executable_path='/usr/src/app/node_modules/chromedriver/bin/chromedriver')

driver = selenium.webdriver.Chrome(options=options, service=service)

driver.get('https://www.taito.co.jp/taito-prize?p=0')
#https://www.teru2teru.com/python/selenium/do-not-time-sleep/
WebDriverWait(driver=driver, timeout=30).until(EC.presence_of_all_elements_located)
soup = BeautifulSoup(driver.page_source, 'html.parser')

#ブラウザの開発ツールから頑張ってclass名を探す必要あり
names = soup.find_all(class_='prize-list-grid-heading')
dates = soup.find_all(class_='prize-list-grid-redtext ng-binding')
#driver.get じゃうまくいかない
for i in range(9):
    try:
        driver.implicitly_wait(300)
        driver.find_element(By.CSS_SELECTOR,"a.ng-scope").click()
        names += soup.find_all(class_='prize-list-grid-heading')
        dates += soup.find_all(class_='prize-list-grid-redtext ng-binding')
    except (selenium.common.exceptions.ElementNotInteractableException, selenium.common.exceptions.StaleElementReferenceException, selenium.common.exceptions.ElementClickInterceptedException):
        pass

#タイトー店舗の入荷プライズ情報をスクレイピングしたい人向け
#10回「もっと見る」をクリックしないと全て表示されない
#for i in range(10):
#    driver.find_element_by_link_text("もっと見る").click()

#names = soup.find_all(class_='section-article-heading--prize-details')
#dates = soup.find_all(class_='section-article-meta-link ng-binding')

#https://teratail.com/questions/165149
#https://qiita.com/taku_hito/items/b78b06c42cb0c28af16e
req_data = []
for (date, name) in zip(dates, names):
    #print(datetime.strptime(re.sub(r"\D","", date.text), '%Y%m%d').date(),name.text)
    req_data += [{
    "date": re.sub(r"\D","", date.text),
    "name": name.text.replace('\u3000', '')
    }]

with open("./public/taito.json",'w') as f:
    json.dump(req_data, f, ensure_ascii=False, indent=4)

driver.quit()
