from bs4 import BeautifulSoup
import selenium 
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import re
import json
from datetime import datetime
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service

options = selenium.webdriver.ChromeOptions()
options.add_argument('--headless')
options.add_argument('--no-sandbox')
options.add_argument("--disable-gpu")
options.add_argument("--disable-dev-shm-usage")
options.add_argument("--disable-dev-tools")
options.add_argument("--hide-scrollbars")
options.add_argument("--single-process")
options.add_argument("--window-size=880x996")
options.binary_location = '/usr/bin/google-chrome'

service = Service(executable_path='/usr/src/app/node_modules/chromedriver/bin/chromedriver')

driver = selenium.webdriver.Chrome(options=options, service=service)

driver.get('https://segaplaza.jp/calendar/?type=prize&period=all&year=2023&month=' + str(datetime.now().month) +'&limit=400')
WebDriverWait(driver=driver, timeout=30).until(EC.presence_of_all_elements_located)
    
html = driver.page_source
soup = BeautifulSoup(html, 'html.parser')

#ブラウザの開発ツールから頑張ってclass名を探す必要あり
names = soup.find_all(class_='el-link el-link-internal itemName unknown')
dates = soup.findAll("span", {"data-v-beba496e": True})

#https://teratail.com/questions/165149
#https://qiita.com/taku_hito/items/b78b06c42cb0c28af16e

req_data = []

driver.quit()

for (date, name) in zip(dates, names):
    req_data += [{
    "date": re.sub(r"\D","", date.text),
    "name": name.text.replace('\u3000', '')
    }]

#json出力
with open("./public/sega.json",'w') as f:
    json.dump(req_data, f, ensure_ascii=False, indent=4)
